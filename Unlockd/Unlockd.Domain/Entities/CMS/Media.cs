﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unlockd.Domain.Entities.CMS
{
    public class Media : BaseEntity
    {
        public string FileName { get; set; }
        public string Thumbnail { get; set; }
        public string FileUrl { get; set; }

    }
}
