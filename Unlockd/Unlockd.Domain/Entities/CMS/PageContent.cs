﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unlockd.Domain.Entities.CMS
{
   public class PageContent:BaseEntity
    {
        public string RawHtmlCode { get; set; }
        public string PageMedia { get; set; }

    }
}
