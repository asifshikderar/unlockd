﻿using System;
using System.Collections.Generic;
using System.Text;
using Unlockd.Domain.Entities.Services;

namespace Unlockd.Domain.Entities.Network
{
  public  class Country:BaseEntity
    {
        public Country()
        {
            NetworkVsCountry = new HashSet<NetworkVsCountry>();
            Contact = new HashSet<Contact>();
        }
        public string Name { get; set; }
        public string FlagIcon { get; set; }
        public virtual ICollection<NetworkVsCountry> NetworkVsCountry { get; set; }
        public virtual ICollection<Contact> Contact { get; set; }
    }
}
