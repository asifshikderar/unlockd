﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unlockd.Domain.Entities.Network
{
   public class NetworkCarrier:BaseEntity
    {
        public NetworkCarrier()
        {
            NetworkVsCountry = new HashSet<NetworkVsCountry>();
        }
        public string NetworkName { get; set; }
        public virtual ICollection<NetworkVsCountry> NetworkVsCountry { get; set; }
    }
}
