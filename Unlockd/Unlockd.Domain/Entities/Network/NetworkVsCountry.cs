﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unlockd.Domain.Entities.Network
{
    public class NetworkVsCountry : BaseEntity
    {
        public long CountryId { get; set; }
        public long NetworkId { get; set; }
        public virtual Country Country { get; set; }
        public virtual NetworkCarrier NetworkCarrier { get; set; }
    }
}
