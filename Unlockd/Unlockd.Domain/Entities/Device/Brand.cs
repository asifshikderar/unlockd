﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unlockd.Domain.Entities.Device
{
    public class Brand : BaseEntity
    {
        public Brand()
        {
            DeviceModel = new HashSet<DeviceModel>();
        }
        public string Name { get; set; }
        public string BrandLogo { get; set; }
        public string ShortDescription { get; set; }
        public virtual ICollection<DeviceModel> DeviceModel { get; set; }

    }
}
