﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unlockd.Domain.Entities.Device
{
  public  class DeviceModel:BaseEntity
    {
        public string Name { get; set; }
        public long BrandId { get; set; }
        public virtual Brand Brand { get; set; }
    }
}
