﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unlockd.Domain.Entities.Blog
{
    public class BlogMediaPosition : BaseEntity
    {
        public BlogMediaPosition()
        {
            BlogMedia = new HashSet<BlogMedia>();
        }
        public string PositionName { get; set; }
        public virtual ICollection<BlogMedia> BlogMedia { get; set; }
    }
}
