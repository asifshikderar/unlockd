﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unlockd.Domain.Entities.Blog
{
    public class Post : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string PrimaryImage { get; set; }
    }
}
