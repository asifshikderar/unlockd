﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unlockd.Domain.Entities.Blog
{
    public class BlogMedia:BaseEntity
    {
        public string MediaFile { get; set; }

        public long BlogMediaPositionId { get; set; }
        public virtual BlogMediaPosition BlogMediaPosition  { get; set; }
    }
}
