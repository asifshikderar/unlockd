﻿using System;

namespace Unlockd.Domain
{
    public class BaseEntity
    {
        public long Id { get; set; }
        public long? Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
    }
}
