﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using Unlockd.Domain.Entities.Auth;

namespace Unlockd.Data
{
    public class UnlockdContext : IdentityDbContext<User>
    {
        public UnlockdContext(DbContextOptions<UnlockdContext> options) : base(options)
        {

        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
